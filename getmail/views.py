# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
import json
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

import requests
import json
from django.conf import settings
import socket

SLACK_NICK = 'webhookbot'
SLACK_URL = 'https://hooks.slack.com/services/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'
SLACK_ICON = ':ghost:'


def send_slack_notification(message, channel="#voicemail", icon=SLACK_ICON):
    try:
        server_nick = socket.gethostname()[-4:]
        msg = "[%s %s]  %s" % (server_nick, SLACK_NICK, message)
        payload = {
            'channel': channel,
            'username': SLACK_NICK,
            "icon_emoji": SLACK_ICON,
            'text': msg,
            'link_names': True
        }
        data = {'payload': json.dumps(payload)}
        requests.post(settings.SLACK_URL, data=data)
    except Exception, ee:
        print ee.message
        return False

@csrf_exempt
def vm_received(request):
    #print ("VM %s" % request)
    #print ("VM %s" % request.body)
    vmurl=request.POST.get("RecordUrl","")
    caller_name=request.POST.get("CallerName","")
    caller_ring=request.POST.get("CallerRing","")
    if caller_ring != "":
        mmsg=("Support call RING from  %s" % (  caller_ring))
        send_slack_notification(mmsg)
        print(mmsg)
        return HttpResponse(status=200)

    from_number=request.POST.get("From","")

    mmsg=("%s, %s, %s" % (vmurl, caller_name, from_number))
    print(mmsg)
    send_slack_notification(mmsg)
    #vmurl=request.
    #jsondata = request.body
    #data = json.loads(jsondata)
    #print ("%s" % jsondata)
    return HttpResponse(status=200)

@csrf_exempt
def call_ringing(request):
    print ("REQ%s" % request)
    print ("BODY %s" % request.body)
    from_number=request.POST.get("Caller","")

    mmsg=("Support call from  %s" % (  from_number))
    print(mmsg)
    #send_slack_notification(mmsg)
    #vmurl=request.
    #jsondata = request.body
    #data = json.loads(jsondata)
    #print ("%s" % jsondata)
    return HttpResponse(status=200)

# Create your views here.
