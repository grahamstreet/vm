import socket
import requests
import json


def send_slack_notification(message):
    try:
        msg = "[%s] %s" % (socket.gethostname(), message)
        payload = {
            'channel': '#notifications',
            'username': 'webhookbot',
            "icon_emoji": ':ghost:',
            'text': msg,
        }
        data = {'payload': json.dumps(payload)}
        requests.post(
            'https://hooks.slack.com/services/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',
             data=data
        )
    except:
        pass


def post_fork(server, worker):
    msg = "Worker spawned (pid: %s)" % worker.pid
    send_slack_notification(msg)
    server.log.info(msg)


def pre_fork(server, worker):
    pass


def pre_exec(server):
    server.log.info("Forked child, re-executing.")


def when_ready(server):
    send_slack_notification("Starting up gunicorn.")
    server.log.info("Server is ready. Spawning workers")


def worker_abort(worker):
    pass


def worker_int(worker):
    pass


def on_reload(server):
    pass


def on_exit(server):
    pass


def worker_exit(server, worker):
    pass

def child_exit(server, worker):
    pass
